package com.funcas.frule.core;

import com.funcas.frule.core.adapter.BaseAdapter;
import com.funcas.frule.core.engine.IRuleEngine;
import com.funcas.frule.core.engine.RuleEntity;
import com.funcas.frule.core.engine.impl.RuleEngine;
import org.junit.Test;

import java.util.HashMap;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;

/**
 * Created by FUNCAS on 2014/9/13.
 */
public class RuleEngineTest {

    @Test
    public void testFireAllRules(){
        IRuleEngine engine = new RuleEngine();

        SimpleAdapter adapter = new SimpleAdapter();
        engine.setAdapter(adapter);
        try {
            Object o = engine.fireAllRules();
            assertEquals(o,"Kitty");
        } catch (InstantiationException e1) {
            e1.printStackTrace();
        } catch (IllegalAccessException e1) {
            e1.printStackTrace();
        }
    }

    @Test
    public void testFireOneRules(){
        IRuleEngine engine = new RuleEngine();

        SimpleAdapter adapter = new SimpleAdapter();
        engine.setAdapter(adapter);
        try {
            Boolean o = (Boolean)engine.fireRuleByName("isUseable",null);
            assertTrue(o);
        } catch (InstantiationException e1) {
            e1.printStackTrace();
        } catch (IllegalAccessException e1) {
            e1.printStackTrace();
        }
    }
}

class SimpleAdapter extends BaseAdapter {

    @Override
    public RuleEntity getRuleEntity() {
        RuleEntity e = new RuleEntity();
        e.setShell(" println(name)\n def isUseable(){\n println('invoke one')\n return true}\n return name;");
        HashMap<String,Object> hs = new HashMap();
        hs.put("name","Kitty");
        e.setRuleVariables(hs);
        return e;
    }
}