package com.funcas.frule.core.engine;

import java.util.Map;

/**
 * 规则变量实体
 * Created by FUNCAS on 2014/9/13.
 */
public class RuleEntity {

    //规则脚本
    private String shell;
    //规则变量
    private Map<String,Object> ruleVariables;

    public String getShell() {
        return shell;
    }

    public void setShell(String shell) {
        this.shell = shell;
    }

    public Map<String, Object> getRuleVariables() {
        return ruleVariables;
    }

    public void setRuleVariables(Map<String, Object> ruleVariables) {
        this.ruleVariables = ruleVariables;
    }
}
