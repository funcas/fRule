package com.funcas.frule.core.engine;

import com.funcas.frule.core.adapter.BaseAdapter;

/**
 * Created by FUNCAS on 2014/9/13.
 */
public interface IRuleEngine {

    /**
     * 执行所有规则集
     * @return
     */
    public Object fireAllRules() throws InstantiationException, IllegalAccessException;

    /**
     * 按名称执行单条规则集
     * @param methodName
     * @return
     */
    public Object fireRuleByName(String methodName,Object[] params) throws InstantiationException, IllegalAccessException;

    /**
     * 设置适配器
     * @param adapter
     */
    public void setAdapter(BaseAdapter adapter);


}
