package com.funcas.frule.core.Exception;

/**
 * Created by FUNCAS on 2014/9/13.
 */
public class RuleException extends Exception{
    public RuleException(){}

    public RuleException(String message){
        super(message);
    }
}
