package com.funcas.frule.core.adapter;

import com.funcas.frule.core.engine.RuleEntity;

/**
 * Created by FUNCAS on 2014/9/13.
 */
public interface IAdapter {

    public RuleEntity getRuleEntity();
}
