package com.funcas.frule.core.adapter;

import com.funcas.frule.core.engine.RuleEntity;

import java.util.Map;

/**
 * Created by FUNCAS on 2014/9/13.
 */
public abstract class BaseAdapter implements IAdapter{

    private RuleEntity entity;

    public RuleEntity getEntity() {
        return entity;
    }

    public void setEntity(RuleEntity entity) {
        this.entity = entity;
    }
}
